﻿using UnityEngine;
using System.Collections;

public class GlobalEvents : MonoBehaviour {

    // Use this for initialization
    public GameObject AI;
    //Peek event settings
    public int peekEventCooldown = 20;
    public int peekEventCurrentDuration = 10;
    public int peekEvent;
    public bool peekEventState = false;

    void Start () {
        peekEvent = peekEventCooldown * 60 + peekEventCurrentDuration * 60;

    }

    // Update is called once per frame
    void Update() //Calculate if peek event occurs
    {
        peekEvent -= 1;
        if (peekEvent < peekEventCurrentDuration * 60)
        {
            if (AI.GetComponent<AiMovement>().RequestAIPreviewState() == false)
            {
                AI.GetComponent<AiMovement>().TogglePreviewMode(); //Allow AI to be seen if event in progress
            }
            peekEventState = true;
        }
        else
        {
            if (AI.GetComponent<AiMovement>().RequestAIPreviewState() == true)
            {
                if (AI.GetComponent<AiMovement>().desiredPreviewMode == false)
                {
                    AI.GetComponent<AiMovement>().TogglePreviewMode(); //Hide AI 
                }
            }
            peekEventState = false;
        }
        if (peekEvent < 0)
        {
            peekEvent += peekEventCooldown * 60 + peekEventCurrentDuration * 60; //Reset
        }
    }
}
