﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerMovement : MonoBehaviour
{

    // Use this for initialization
    public GameObject laserTemplate;
    public GameObject pointControllerObject;

    float health = 100.0f;
    Vector3 currentPos;
    Vector3 forceVector;
    bool posX = false;
    bool posY = false;
    int stepsToMove = 0;

    void Start()
    {
        currentPos = transform.position;
    }

    public void hit() //Hit by enemy
    {
        health = health - 1.5f;
        if (health < 0.1)
        {
            gameObject.SetActive(false);
            health = 0;
        }
    }

    public float RequestHealth() //Return player health
    {
        return health;
    }

    public void MoveToPoint(Vector3 newPos) //Calculate movement vector
    {
        if (newPos.x < currentPos.x)
        {
            forceVector.x = (currentPos.x - newPos.x) / 10;
            posX = false;
        }
        else
        {
            forceVector.x = (newPos.x - currentPos.x) / 10;
            posX = true;
        }
        if (newPos.y < currentPos.y)
        {
            forceVector.y = (currentPos.y - newPos.y) / 10;
            posY = false;
        }
        else
        {
            forceVector.y = (newPos.y - currentPos.y) / 10;
            posY = true;
        }
        stepsToMove = 10;
    }

    bool Moving()
    {
        if (stepsToMove > 0) //Move Between points
        {
            if (posX == true)
            {
                currentPos.x += forceVector.x;
            }
            else
            {
                currentPos.x -= forceVector.x;
            }
            if (posY == true)
            {
                currentPos.y += forceVector.y;
            }
            else
            {
                currentPos.y -= forceVector.y;
            }
            transform.position = currentPos;
            stepsToMove -= 1;
        }
        if (stepsToMove > 0)
        {
            return false;
        }
        return true;
    }

    // Update is called once per frame
    void Update()
    {
        if (health < 100) //Regeneration of health 
        {
            health += 0.2f;
        }

        Moving();

        if (Input.GetKey("a")) //Rotation
        {
            transform.Rotate(0, 0, 5);
        }

        if (Input.GetKey("d"))
        {
            transform.Rotate(0, 0, -5);
        }

        if (Input.GetKey("space")) //Shooting
        {
            GameObject newLaser = (GameObject)Instantiate(laserTemplate, transform.position + (transform.TransformDirection(Vector3.down) / 2.2f), transform.rotation);
            newLaser.GetComponent<Rigidbody>().AddForce(transform.TransformDirection(Vector3.down) * 800);
        }

        if (Input.GetMouseButtonDown(0)) //Player click, call point controller to check if player clicek point
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                pointControllerObject.gameObject.GetComponent<PointController>().PlayerRayHit(hit);
            }
        }
    }
}
