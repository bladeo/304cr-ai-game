﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UiScript : MonoBehaviour {

    // Use this for initialization
    public Text HealthText;
    public Text AiText;
    public Text winTxt;
    public Text peekEventTxt;

    public GameObject player;
    public GameObject AI;
    public GameObject globalEventsHolder;

    void Start () {
	    if (AI.GetComponent<AiMovement>().RequestAIPreviewState() == true)
        {
            AiText.gameObject.SetActive(true);
        } else
        {
            AiText.gameObject.SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (globalEventsHolder.GetComponent<GlobalEvents>().peekEvent < globalEventsHolder.GetComponent<GlobalEvents>().peekEventCurrentDuration * 60) { //Show peek event cooldown and show if active
            peekEventTxt.text = "Peek Event Active";
        } else {
            int cooldown = (globalEventsHolder.GetComponent<GlobalEvents>().peekEvent - globalEventsHolder.GetComponent<GlobalEvents>().peekEventCurrentDuration * 60) / 60;
            peekEventTxt.text = "Peek Event in: " + (int)cooldown;
        }

        //Show player and AI health
        HealthText.text = "Health: " + (int)player.GetComponent<PlayerMovement>().RequestHealth();
        AiText.text = "AI Health: " + (int)AI.GetComponent<AiMovement>().RequestHealth();
        if ((int)player.GetComponent<PlayerMovement>().RequestHealth() <= 0)
        {
            winTxt.text = "AI Wins";
            AI.SetActive(false);
        } else if ((int)AI.GetComponent<AiMovement>().RequestHealth() <= 0)
        {
            winTxt.text = "You Win";
        }
        else
        {
            winTxt.text = "";
        }
        if (AI.GetComponent<AiMovement>().RequestAIPreviewState() == true) //Only show health if AI can be seen
        {
            AiText.gameObject.SetActive(true);
        }
        else
        {
            AiText.gameObject.SetActive(false);
        }
    }
}
