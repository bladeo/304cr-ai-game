﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerLaser : MonoBehaviour
{
    // Use this for initialization
    public GameObject player;
    public GameObject ai;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnCollisionEnter(Collision collision) //Test collision of lasar with player and AI
    {
        if (collision.gameObject.name != gameObject.name)
        {
            Destroy(gameObject);
        } else if (collision.gameObject.name == gameObject.name)
        {
            Destroy(gameObject, 2.0f);
        }

        if (collision.gameObject.name == player.name)
        {
            player.gameObject.GetComponent<PlayerMovement>().hit();
        } else if (collision.gameObject.name == ai.name)
        {
            ai.gameObject.GetComponent<AiMovement>().hit();
        }

    }
}
