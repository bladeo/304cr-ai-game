﻿using UnityEngine;
using System.Collections;

public class AiMovement : MonoBehaviour {

    // Use this for initialization
    public GameObject laserTemplate;
    public GameObject pointControllerObject;
    public GameObject player;
    public GameObject globalEventsObject;
    bool aiPreviewMode = false;
    public bool desiredPreviewMode = false;

    float health = 100;
    Vector3 currentPos;
    Vector3 forceVector;
    Vector3 newPos;
    bool posX = false;
    bool posY = false;
    int stepsToMove = 0;
    int reactionDelay = 0;
    int reactionSpeed = 2;  //These set the difficulty of the AI
    int rotationSpeed = 3; //5 or 10
    int currentZRotation = 0;
    int rotationTarget = 0;

    public void TogglePreviewMode() //Toggle AI visibility
    {
        if (aiPreviewMode == false)
        {
            aiPreviewMode = true;
            gameObject.GetComponent<SpriteRenderer>().enabled = true;
        }
        else
        {
            aiPreviewMode = false;

        }
    }
    public void ButtonPreviewMode() //Toggle if the AI should be visible or not based on UI button
    {
        if (desiredPreviewMode)
        {
            desiredPreviewMode = false;
        }
        else
        {
            desiredPreviewMode = true;
        }
    }

    public bool RequestAIPreviewState() //Request if the AI is visible or not
    {
        return aiPreviewMode;
    }

    void Start()
    {
        currentPos = transform.position;
    }

    public void hit() //Hit by enemy
    {
        health = health - 1.5f;
        if (health < 0.1)
        {
            gameObject.SetActive(false);
        }
    }

    public float RequestHealth() //Request the current health of the AI
    {
        return health;
    }

    public void MoveToPoint(Vector3 newPos1) //Calculate movement vector
    {
        newPos = newPos1;

        if (newPos.x < currentPos.x)
        {
            forceVector.x = (currentPos.x - newPos.x) / 10;
            posX = false;
        }
        else
        {
            forceVector.x = (newPos.x - currentPos.x) / 10;
            posX = true;
        }
        if (newPos.y < currentPos.y)
        {
            forceVector.y = (currentPos.y - newPos.y) / 10;
            posY = false;
        }
        else
        {
            forceVector.y = (newPos.y - currentPos.y) / 10;
            posY = true;
        }
        stepsToMove = 10;
    }

    bool Moving() //Move Between points
    {
        if (stepsToMove > 0) //Move if greater than 0
        {
            if (posX == true)
            {
                currentPos.x += forceVector.x;
            }
            else
            {
                currentPos.x -= forceVector.x;
            }
            if (posY == true)
            {
                currentPos.y += forceVector.y;
            }
            else
            {
                currentPos.y -= forceVector.y;
            }
            transform.position = currentPos;
            stepsToMove -= 1;
        }
        if (stepsToMove > 0)
        {
            return true;
        }
        return false;
    }


    bool Delay() //Delay AI reactions
    {
        if (reactionDelay > 0)
        {
            reactionDelay -= reactionSpeed;
            return true;
        }
        return false;
    }

    // Update is called once per frame
    void Update()
    {
        if (health < 100) //Regeneration of health 
        {
           health += 0.2f;
        }

        RotateToAngle(rotationTarget); //Rotate to face target

        if (CastRayAtPlayer() == true) //If player is visible
        {
            if (aiPreviewMode == false) { //Allow AI to be seen
                gameObject.GetComponent<SpriteRenderer>().enabled = true;
            }
            if (Moving() != true) //AI is moving
            {
                if (health > 50)
                {
                    Attack();
                }
                else
                {
                    if (Delay() == false) //Reaction Delay
                    {
                        Run();
                    }
                }
            }
        }
        else
        {
            if (aiPreviewMode == false) { //Stop AI being seen
                gameObject.GetComponent<SpriteRenderer>().enabled = false;
            }
            if (Moving() == true)
            {
                RotateAroundCorner(); //Rotate AI round corners (more realistic)
            }
            else 
            {
                if (Delay() == false)
                {
                    if (health > 50)
                    {
                        if (globalEventsObject.GetComponent<GlobalEvents>().peekEventState == true) //If there is a peek event
                        {
                            MoveToPlayer();
                        }
                        else
                        {
                            Search();
                        }
                    }
                    else
                    {
                        Run();
                    }
                }
            }
        }     
    }

    bool CastRayAtPlayer() //Function to check if player and AI in line of sight
    {
        RaycastHit hit;
        if (transform.position != player.transform.position) 
        {
            Physics.Raycast(transform.position, player.transform.position - transform.position, out hit);
            Debug.DrawRay(transform.position, player.transform.position - transform.position, Color.green);
            if (hit.collider.gameObject.name == player.gameObject.name)
            {
                return true;
            }
        }
        return false;
    }

    void Search() //Calls the point handling script to get new search point
    {
        Vector3 newPos = pointControllerObject.GetComponent<PointController>().AiSearchPointSelect();
        MoveToPoint(newPos);
        reactionDelay += 50;
    }

    void Run()//Calls the point handling script to get new run point
    {
        Vector3 newPos = pointControllerObject.GetComponent<PointController>().AiRun();
        MoveToPoint(newPos);
        reactionDelay += 50;
    }

    void Attack() //Find angle to rotate and shoot if pointing at targer
    {
        Vector3 dir = player.transform.position - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg; //Calculate angle
        angle += 90; //Rotate angle to infront of gun
        rotationTarget = (int)angle; 
        if (RotateToAngle(rotationTarget) == true) //Check if angle is set
        {
            Shoot();
        }
    }

    void MoveToPlayer() // Call the point handling script to use Dijkstra to move
    {
        Vector3 newPos = pointControllerObject.GetComponent<PointController>().AiMoveDijkstra();
        MoveToPoint(newPos);
        reactionDelay += 50;
    }

    void Shoot() //Shoot a laser
    {
        GameObject newLaser = (GameObject)Instantiate(laserTemplate, transform.position + (transform.TransformDirection(Vector3.down) / 2.2f), transform.rotation);
        newLaser.GetComponent<Rigidbody>().AddForce(transform.TransformDirection(Vector3.down) * 800);
    }

    void RotateAroundCorner() //Rotate towards new point
    {
        float vertDist = 0;
        float horizDist = 0;
        int vertAngle = 0;
        int horizAngle = 0;
        if (newPos.y < currentPos.y)
        {
            vertDist = currentPos.y - newPos.y;
            vertAngle = 0;
        }
        if (newPos.y > currentPos.y)
        {
            if (newPos.y - currentPos.y > vertDist) { vertDist = newPos.y - currentPos.y; vertAngle = 180; }
        }
        if (newPos.x < currentPos.x)
        {
            horizDist = currentPos.x - newPos.x;
            horizAngle = 270;
        }
        if (newPos.x > currentPos.x)
        {
            if (newPos.x - currentPos.x > horizDist) { horizDist = newPos.x - currentPos.x; horizAngle = 90; }
        }
        if (horizDist > vertDist)
        {
            rotationTarget = horizAngle;
        }
        else
        {
            rotationTarget = vertAngle;
        }
    }

    bool RotateToAngle(int angle) //Rotate towards requested angle
    {
        if (currentZRotation > 360)
        {
            currentZRotation -= 360;
        }
        else if (currentZRotation < 0)
        {
            currentZRotation += 360;
        }

        if (currentZRotation != angle)
        {
            if ((currentZRotation - angle) < 0){
                transform.Rotate(0, 0, rotationSpeed);
                currentZRotation += rotationSpeed;
            }
            else
            {
                transform.Rotate(0, 0, -rotationSpeed);
                currentZRotation -= rotationSpeed;
            }
        }
        if (currentZRotation < angle + 5 && currentZRotation > angle - 5)
        {
            return true;
        }
        return false;
    }
}
